package eu.dindoffer.weakhashmap.example.gui;

import eu.dindoffer.weakhashmap.example.core.PieLoader;
import javafx.scene.image.Image;

/**
 *
 * @author Martin Dindoffer <contact@dindoffer.eu>
 */
public interface IView {
    
    void openImageViewer(PieLoader.MetaData meta, Image img);
}

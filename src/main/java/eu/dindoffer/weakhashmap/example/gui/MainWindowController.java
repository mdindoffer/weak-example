package eu.dindoffer.weakhashmap.example.gui;

import eu.dindoffer.weakhashmap.example.core.PieLoader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author Martin Dindoffer <contact@dindoffer.eu>
 */
public class MainWindowController implements IView {

    private final PieLoader pieLoader = new PieLoader();

    @FXML
    private VBox myRoot;
    @FXML
    private Button btnShowHashMapSize;
    @FXML
    private Label lblSizeKey;
    @FXML
    private Label lblSizeValue;
    @FXML
    private Button btnShowPrettiestPie;

    @FXML
    private void onbtnShowPrettiestPie(ActionEvent event) {
        new Thread(() -> {
            List<String> pies = new LinkedList<>();
            pies.add("apple_pie.bmp");
            pies.add("cherry_pie.jpg");
            pies.add("chocolate_pie.jpg");
            pieLoader.loadAndProcessPies(pies, this);
        }).start();
    }

    @FXML
    private void onBtnShowHashMapSize(ActionEvent event) {
        lblSizeValue.setText(String.valueOf(pieLoader.getWeakHashMapSize()));
    }

    @Override
    public void openImageViewer(PieLoader.MetaData meta, Image img) {
        Platform.runLater(() -> {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getClassLoader().getResource("fxml/ImageViewer.fxml"));
            Parent root = null;
            try {
                root = (Parent) fxmlLoader.load();
            } catch (IOException ex) {
            }
            Stage stage = new Stage();
            stage.setMaximized(false);
            stage.setTitle(meta.getName() + " loaded on " + meta.getTimeOfCreation());
            ImageViewerController ivController = fxmlLoader.<ImageViewerController>getController();
            ivController.setImage(img);
            stage.setScene(new Scene(root));
            stage.show();
        });
    }
}

package eu.dindoffer.weakhashmap.example.gui;

import javafx.fxml.FXML;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 * FXML Controller class
 *
 * @author Martin Dindoffer <contact@dindoffer.eu>
 */
public class ImageViewerController {

    @FXML
    private ImageView imvImage;

    public void setImage(Image img) {
        imvImage.setImage(img);
    }
}

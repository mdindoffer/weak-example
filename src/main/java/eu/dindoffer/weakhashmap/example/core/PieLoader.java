package eu.dindoffer.weakhashmap.example.core;

import eu.dindoffer.weakhashmap.example.badlib.BadLib;
import eu.dindoffer.weakhashmap.example.badlib.ProprietaryImage;
import eu.dindoffer.weakhashmap.example.gui.IView;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;

/**
 *
 * @author Martin Dindoffer <contact@dindoffer.eu>
 */
public class PieLoader {

    private final Map<ProprietaryImage, MetaData> imageMetas = new WeakHashMap<>();

    public void loadAndProcessPies(List<String> imageNames, IView callback) {
        System.out.println("Loading images...");
        List<ProprietaryImage> imgs = new LinkedList<>();
        for (String imageName : imageNames) {
            LocalTime timeOfImageLoad = LocalTime.now();
            ProprietaryImage img = new ProprietaryImage(this.getClass().getClassLoader()
                    .getResourceAsStream(imageName));
            imageMetas.put(img, new MetaData(imageName, timeOfImageLoad));
            imgs.add(img);
        }

        new Thread(() -> {
            ProprietaryImage prettiestImg = BadLib.findPrettiestImage(imgs);
            callback.openImageViewer(imageMetas.get(prettiestImg), prettiestImg);
        }).start();
    }
    
    public int getWeakHashMapSize() {
        return imageMetas.size();
    }

    public static class MetaData {

        private final String name;
        private final LocalTime timeOfCreation;

        public MetaData(String name, LocalTime timeOfCreation) {
            this.name = name;
            this.timeOfCreation = timeOfCreation;
        }

        public String getName() {
            return name;
        }

        public LocalTime getTimeOfCreation() {
            return timeOfCreation;
        }
    }
}

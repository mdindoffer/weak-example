package eu.dindoffer.weakhashmap.example.badlib;

import java.io.InputStream;
import javafx.scene.image.Image;

/**
 *
 * @author Martin Dindoffer <contact@dindoffer.eu>
 */
public final class ProprietaryImage extends Image {

    public ProprietaryImage(InputStream is) {
        super(is);
    }
}

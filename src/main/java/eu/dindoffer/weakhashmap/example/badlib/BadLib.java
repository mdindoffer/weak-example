package eu.dindoffer.weakhashmap.example.badlib;

import java.util.List;
import java.util.Random;

/**
 *
 * @author Martin Dindoffer <contact@dindoffer.eu>
 */
public class BadLib {

    private BadLib() {
    }

    public static ProprietaryImage findPrettiestImage(List<ProprietaryImage> images) {
            try {
                Thread.sleep(500);
            } catch (InterruptedException ex) {
            }
            int index = new Random().nextInt(images.size());
            return images.get(index);
    }
}
